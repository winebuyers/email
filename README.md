```html
<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tbody>
        <tr>
            <td border="0" cellpadding="0" cellspacing="0">
                <img src="https://www.winebuyers.com/bundles/app/img/only_logo_black_rectangule.png" height="" width="80" alt="Winebuyers Limited Icon">
            </td>
        </tr>
        <tr>
            <td height="30" style="font-family:Arial, sans-serif; font-size:14px; font-weight: normal; color: #a68d6b">
                Santiago Barbat
            </td>
        </tr>
        <tr>
            <td height="10" style="font-family: Arial, sans-serif; font-size:14px; font-style:bold;">
                <span style="font-size:14px;">Head Developer | </span>
                <strong>Winebuyers</strong>
            </td>
        </tr>
        <tr>
            <td height="58" style="font-family:Helvetica, Arial, sans-serif; font-size:16px; color:#4d4d4e;">
                800 N. Magnolia Ave, Suite 1400 | Orlando, Fl 32803
                <br>
                Office 407.898.1961
            </td>
        </tr>
        <tr>
            <td class="hover" height="60" style="font-family:Helvetica, Arial, sans-serif; font-size:16px; color:#d0292d;">
                <a href="mailto:santiago@winebuyers.com" onmouseover="this.style.color='#71080a'" onmouseout="this.style.color='#d0292d'" style="color:#d0292d; ">santiago@winebuyers.com</a>
                <br>
                <a href="https://www.winebuyers.com/" onmouseover="this.style.color='#71080a'" onmouseout="this.style.color='#d0292d'" target="_blank" style="color:#d0292d;">winebuyers.com</a>
            </td>
        </tr>
        <tr>
            <td height="55">
                <a href="https://www.facebook.com/winebuyersofficial" target="_blank"><img src="https://www.solodev.com/assets/email-signature/facebook-email-icon.jpg" alt="Facebook Icon"></a>
                <a href="https://www.linkedin.com/company/winebuyers" target="_blank"><img src="https://www.solodev.com/assets/email-signature/linkedin-email-icon.jpg" alt="LinkedIn Icon"></a>
                <a href="https://twitter.com/WinebuyersClub" target="_blank"><img src="https://www.solodev.com/assets/email-signature/twitter-email-icon.jpg" alt="Twitter Icon"></a>
            </td>
        </tr>
    </tbody>
</table>
```